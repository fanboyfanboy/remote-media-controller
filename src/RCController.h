/**
*
*
* RCController tapes together RCNetworkManager and RCPlaybackManager.  Information from one is being processed and sent to the other.
* It is being inherited from the main function on the daemon.
*
* Authors
* FanboyFanboy, H6nry
*/

#import <Foundation/Foundation.h>
#import "Network/RCNetworkManager.h"
#import "Playback/RCPlaybackManager.h"

@interface RCController : NSObject <RCNetworkManagerDelegate, RCPlaybackManagerDelegate> {
	RCNetworkManager *_networkManager;
	RCPlaybackManager *_playbackManager;
}
@property BOOL enabled;
@property (nonatomic, copy) NSString *targetIP;
@property (nonatomic) NSDictionary *nowPlayingInformation;

+(id) sharedInstance; //The class for sending and recieving messages
-(void) loadPreferences;
//Own methods here
@end