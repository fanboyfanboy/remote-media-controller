#import "RCController.h"
#import <CoreFoundation/CoreFoundation.h>

static RCController *_sharedInstance = nil;
static void loadPrefs(){
	[[RCController sharedInstance] loadPreferences];
}

@implementation RCController : NSObject

+(id) sharedInstance {
	@synchronized(self) {
		if (!_sharedInstance) {
			_sharedInstance = [[self alloc] init];
		}
		return _sharedInstance;
	}
}



// Own methods to be implemented here
-(id) init {
	if(self = [super init]){
		loadPrefs();
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, (CFNotificationCallback)loadPrefs, CFSTR("team.fanboyfanboy-h6nry.remotemediacontrol.rmcPrefs/settingschanged"), NULL, CFNotificationSuspensionBehaviorCoalesce);
		self.enabled = 0;
		self.targetIP = @"255.255.255.255";
		_networkManager = [[RCNetworkManager alloc] init];
		_playbackManager = [[RCPlaybackManager alloc] init];
		if (!(_networkManager) || !(_playbackManager))
			NSLog(@"RMC: failed at initializing %@ and %@.", _networkManager, _playbackManager);

		_networkManager.delegate = self;
		_playbackManager.delegate = self;
		[self loadPreferences];

		[_playbackManager beginControllingPlayback];

		sleep(5);
		NSLog(@"RMC: Attempting to play music.");
		[_playbackManager play];
		sleep(15);
		NSLog(@"RMC: Attempting to skip to the next song.");
		[_playbackManager nextTrack];
		sleep(15);
		NSLog(@"RMC: Attempting to pause.");
		[_playbackManager pause];
		sleep(5);
		NSLog(@"RMC: Attempting to play again.");
		[_playbackManager play];
		sleep(20);
		NSLog(@"RMC: Stopping playback.");
		[_playbackManager stopPlayback];

	}
	return self;
}

// RCNetworkManager delegate
-(void) receivedMessage:(NSDictionary *)message {
	return;
}

-(void) loadPreferences{
	NSDictionary *prefs = [NSDictionary dictionaryWithContentsOfFile:@"/var/mobile/Library/Preferences/team.fanboyfanboy-h6nry.remotemediacontrol.rmcPrefs.plist"];
	self.enabled = [[prefs objectForKey:@"enable"] boolValue];
	self.targetIP = [prefs objectForKey:@"ip"];
}

/**
* RCPlaybackManager delegate.  It processes the information and calls the corresponding methods in RCPlaybackManager 
*
* @param information The dictionary containing the calls to make
*/
-(void) updateNowPlayingInformation:(NSDictionary *)information {
	NSLog(@"RMC: Updating Client now playing information.");
	_nowPlayingInformation = [[NSDictionary alloc] initWithDictionary:information];

	if(_nowPlayingInformation == nil){
		NSLog(@"RMC: RCController,  Failed to get now playing info!");
		return;
	}
	NSLog(@"RMC: RCController, Got now playing info successfully.");
	NSLog(@"RMC: Title is %@ ", [_nowPlayingInformation objectForKey:@"Title"]);
	//Whenever something changes, we should here update the information on any clients (controllers).
	return;
}

@end