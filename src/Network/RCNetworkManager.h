/*
 RCNetworkManager is the class to communicate with the network and the world wide web.
 It provides methods to send and receive messages and a protocol which provides an easy access to received messages.
 */

#import <Foundation/Foundation.h>

@protocol RCNetworkManagerDelegate // RCNetworManager protocol, every class sending messages should implement this.
-(void) receivedMessage:(NSDictionary *)message;
@end


@interface RCNetworkManager : NSObject {
}
@property (assign) id<RCNetworkManagerDelegate> delegate; // The delegate protocol messages to be sent to.

//-(void) sendMessage:(NSDictionary *)message;
@end