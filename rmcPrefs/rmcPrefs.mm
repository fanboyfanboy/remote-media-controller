#import <Preferences/Preferences.h>
#import <UIKit/UIKit.h>
#define RMC_VERSION_STRING @"RMC/1.0-dev"

@class PSRootController;

@interface rmcPrefsListController: PSListController {
}
@end

@implementation rmcPrefsListController //The main list controller
- (id)specifiers {
	if(_specifiers == nil) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"rmcPrefs" target:self] retain];
	}
	return _specifiers;
}

-(void) fftwitter{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/fanboyfanboydev/"]];
}
-(void) h6twitter{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/H6nry_/"]];
}

-(void) ffmail{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:fanboyfanboydev@hotmail.com"]];
}

-(void) h6mail{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:henry.anonym@gmail.com"]];
}

-(void) ffwebsite{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://fanboyfanboy.bitbucket.org/"]];
}

-(void) h6website{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://h6nry.github.io/"]];
}

-(void) apply{
	system("killall -9 backboardd"); //Backboardd is better for killing the springboard than springboard itself
}
@end

// vim:ft=objc
